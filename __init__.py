# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
from trytond.pool import Pool
from . import account


def register():
    Pool.register(
        account.Move,
        account.MoveLine,
        account.Payment,
        module='account_payment_amount_store', type_='model')
