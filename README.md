datalife_account_payment_amount_store
=====================================

The account_payment_amount_store module of the Tryton application platform.

[![Build Status](http://drone.datalifeit.es:8050/api/badges/datalifeit/trytond-account_payment_amount_store/status.svg)](http://drone.datalifeit.es:8050/datalifeit/trytond-account_payment_amount_store)

Installing
----------

See INSTALL


License
-------

See LICENSE

Copyright
---------

See COPYRIGHT
